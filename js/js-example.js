let userName=prompt("what is your name?");
let userAge=prompt("what is your age");

while (!userName || isNaN(userAge) || !userAge){
    userName=prompt("what is your name?", `${userName}`);
    userAge=prompt("what is your age", `${userAge}`);
}
const welcomeText = `Welcome, ${userName}`;
const notAllowed = "You are not allowed to visit this website.";

if (userAge<18){
    alert(notAllowed);
}
else if (userAge<22){
   if (confirm('Are you sure you want to continue?')){
        alert(welcomeText);
    }
    else{
        alert(notAllowed);
    }
}
else{
    alert(welcomeText);
}
